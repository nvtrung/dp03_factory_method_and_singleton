﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HelloSingleton
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnXemTroGiup_Click(object sender, EventArgs e)
        {
            //var frm = new FormHelp();
            //frm.Show();

            var frm = FormHelp.getInstance();
            frm.Show();
            frm.BringToFront();
        }

        private void btnCong_Click(object sender, EventArgs e)
        {
            var sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            var a = Int16.Parse(txtA.Text);
            var b = Int16.Parse(txtB.Text);
            var kq = SmartEngine.cộng(a, b);
            sw.Stop();

            MessageBox.Show(String.Format("Kết quả là {0}. Mất {1}ms để thực hiện", kq, sw.ElapsedMilliseconds));
        }
    }
}
