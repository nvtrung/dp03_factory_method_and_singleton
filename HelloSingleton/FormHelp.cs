﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HelloSingleton
{
    public partial class FormHelp : Form
    {
        private static FormHelp _instance = null;

        public static FormHelp getInstance()
        {
            if (_instance == null || _instance.IsDisposed)
            {
                // Giả sử thao tác này mất 10s
                _instance = new FormHelp();
                _instance.Text = "Đối tượng được tạo vào lúc " + DateTime.Now.ToString();                
            }

            return _instance;
        }

        private FormHelp()
        {
            InitializeComponent();
        }
    }
}
